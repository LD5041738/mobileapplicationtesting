package utils;

public class Constants 
{
	private static final PropertyFileReader propReader= new PropertyFileReader();
	public static final String url= propReader.getURL();
	public static final String PROJECT_PATH = System.getProperty("user.dir");
	public static final String testDataPath= propReader.getTestData();
	public static final String browser= propReader.getBrowser();
	
	public static final String chromePath= propReader.getChromeDriverPath();
	
	public static final String firefoxPath= propReader.getFirefoxDriverPath();
	
	public static final String reportPath= propReader.getReportPath();
	public static final String documentName= propReader.getDocumentName();
	public static final String reportName= propReader.getReportName();
}
