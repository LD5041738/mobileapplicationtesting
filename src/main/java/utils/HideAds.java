package utils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import driver.DriverClass;

public class HideAds extends DriverClass
{
	public static void hideAds()
	{
		WebElement element = driver.findElement(By.xpath("//*[@id='fixedban']"));
		((JavascriptExecutor)driver).executeScript("arguments[0].style.visibility='hidden'", element);
	}
}
