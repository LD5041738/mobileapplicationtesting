package utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertyFileReader 
{
	private static Properties prop = new Properties();
	
	PropertyFileReader() 
	{
		try 
		{
			prop.load(new FileInputStream("./resources/Config.properties"));
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
	}
	
	public String getHeadlessOption()
	{
		return prop.getProperty("headlessOption");
	}
	
	public String getBrowser()
	{
		String value=prop.getProperty("browser");
		if(value.equalsIgnoreCase("chrome") && value!="")
		{
			return value;
		}
		else if (value.equalsIgnoreCase("firefox") && value!="")
		{
			return value;
		}
		else if (value.equalsIgnoreCase("headless"))
		{
			String headlessOptions=getHeadlessOption();
			if(headlessOptions.equalsIgnoreCase("chromeHeadless") && headlessOptions!="")
			{
				return headlessOptions;
			}
			else if(headlessOptions.equalsIgnoreCase("firefoxHeadless") && headlessOptions!="")
			{
				return headlessOptions;
			}
			else
			{
				return value;
			}
		}
		return value; 
	}
	
	public String getURL()
	{
		return prop.getProperty("URL");
	}
	
	public String getTestData()
	{
		return prop.getProperty("testDataFilePath");
	}
	
	public String getChromeDriverPath()
	{
		return prop.getProperty("chromeDriver");
	}
	
	public String getFirefoxDriverPath()
	{
		return prop.getProperty("gekoDriver");
	}
	
	public String getReportPath()
	{
		return prop.getProperty("ExtentReportPath");
	}
	
	public String getDocumentName()
	{
		return prop.getProperty("DocumentName");
	}
	
	public String getReportName()
	{
		return prop.getProperty("ReportName");
	}
}
