package utils;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import driver.BasePage;

public class Scroller extends BasePage
{
	public static Scroller getInstance()
	{
		return new Scroller();
	}
	
	public Scroller scrollDownByPixels()
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("window.scrollBy(0,350)", "");
	    return this;
	}
	
	public Scroller scrollUpByPixels()
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("window.scrollBy(0,-350)", "");
	    return this;
	}
	
	public Scroller scrollUntilElementIsNotVisible(WebElement element)
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", element);
		return this;
	}
	
	public Scroller scrollToBottomOfPage()
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		return this;
	}
}
