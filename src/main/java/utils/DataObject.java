package utils;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class DataObject 
{
	String key;
	public static HashMap<String, LinkedHashMap<String, String>> ExcelObject = new LinkedHashMap<String, LinkedHashMap<String, String>>();

	public void setKey(String key)
	{
		this.key=key;
	}
	
	public String getKey()
	{
		return key;
	}
	
	public void setExcelObject(HashMap<String,LinkedHashMap<String,String>> excelObject)
	{
		ExcelObject=excelObject;
	}
	
	public static HashMap<String,LinkedHashMap<String, String>> getExcelObject()
	{
		return ExcelObject;
	}
	
	public static String getVariable(String ColumnName, String testcase) 
	{
		String value = DataObject.getExcelObject().get(testcase).get(ColumnName).toString();
		return value;
	}
	
}
