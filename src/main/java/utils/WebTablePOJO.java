package utils;

public class WebTablePOJO 
{
	private static String firstName;
	private static String lastName;
	private static String age;
	private static String email;
	private static String salary;
	private static String department;
	
	public String getFirstName() 
	{
		return firstName;
	}
	public void setFirstName(String firstName) 
	{
		this.firstName = firstName;
	}
	public String getLastName() 
	{
		return lastName;
	}
	public void setLastName(String lastName) 
	{
		this.lastName = lastName;
	}
	public String getAge() 
	{
		return age;
	}
	public void setAge(String age) 
	{
		this.age = age;
	}
	public String getEmail() 
	{
		return email;
	}
	public void setEmail(String email) 
	{
		this.email = email;
	}
	public String getSalary() 
	{
		return salary;
	}
	public void setSalary(String salary) 
	{
		this.salary = salary;
	}
	public String getDepartment() 
	{
		return department;
	}
	public void setDepartment(String department) 
	{
		this.department = department;
	}
}
