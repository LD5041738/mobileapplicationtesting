package elements;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.internal.WrapsElement;
import org.openqa.selenium.remote.RemoteWebElement;

public class CommonElements extends RemoteWebElement implements IName, WrapsElement , WebElement
{
	private WebElement element;
	private String elementName;
	
	protected CommonElements(WebElement element, String elementName)
	{
		this.element=element;
		this.elementName=elementName;
	}
	
	@Override
	public void clear() {
		// TODO Auto-generated method stub
		super.clear();
	}
	
	@Override
	public String getAttribute(String name) {
		// TODO Auto-generated method stub
		return super.getAttribute(name);
	}
	
	@Override
	public <X> X getScreenshotAs(OutputType<X> outputType) throws WebDriverException {
		// TODO Auto-generated method stub
		return super.getScreenshotAs(outputType);
	}
	
	@Override
	public String getText() {
		// TODO Auto-generated method stub
		return super.getText();
	}
	
	@Override
	public boolean isDisplayed() {
		// TODO Auto-generated method stub
		return super.isDisplayed();
	}
	
	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return super.isEnabled();
	}
	
	@Override
	public boolean isSelected() {
		// TODO Auto-generated method stub
		return super.isSelected();
	}
	
	@Override
	public WebElement findElement(By by) {
		// TODO Auto-generated method stub
		return super.findElement(by);
	}
	
	@Override
	public List<WebElement> findElements(By by) {
		// TODO Auto-generated method stub
		return super.findElements(by);
	}
	
	@Override
	public String getCssValue(String propertyName) {
		// TODO Auto-generated method stub
		return super.getCssValue(propertyName);
	}
	
	
	@Override
	public final WebElement getWrappedElement() 
	{
		return element;
	}

	@Override
	public String getName() 
	{
		return elementName;
	}

}
