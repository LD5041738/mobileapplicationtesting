package elements;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;

public class InputElement extends CommonElements
{
	private static Logger logger = Logger.getLogger(InputElement.class);
	public InputElement(WebElement element, String elementName) 
	{
		super(element, elementName);
	}
	
	 public void setValue(String input)
	 {
		 System.out.println(String.format("set ['%s'] into ['%s'] text box",input,getName()));
		 getWrappedElement().clear();
		 getWrappedElement().sendKeys(input);
		 logger.info(String.format("set ['%s'] into ['%s'] text box",input,getName()));
	 }
	 public void appendValue(String input)
	 {
		 System.out.println(String.format("set ['%s'] in to [%s] textbox",input,getName()));
		 getWrappedElement().sendKeys(input);
		 logger.info(String.format("set ['%s'] into ['%s'] text box",input,getName()));
	 }

}
