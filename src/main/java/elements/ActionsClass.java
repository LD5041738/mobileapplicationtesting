package elements;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import driver.BasePage;
import driver.DriverClass;

public class ActionsClass extends CommonElements
{
	private static Logger logger = Logger.getLogger(ActionsClass.class);
	Actions action = new Actions(BasePage.driver);
	public ActionsClass(WebElement element, String elementName)
	{
		super(element, elementName);
	}
	
	public ActionsClass doubleClick()
	{
		
		System.out.println(String.format("doubleClick on ['%s'] button",getName()));
		action.doubleClick(getWrappedElement()).build().perform();
		logger.info(String.format("doubleClick on ['%s'] button",getName()));
		return this;
	}
	
	public ActionsClass rightClick()
	{
		System.out.println(String.format("rightClick on ['%s'] button",getName()));
		action.contextClick(getWrappedElement()).build().perform();
		logger.info(String.format("rightClick on ['%s'] button",getName()));
		return this;
	}
	
	public ActionsClass simpleClick()
	{
		System.out.println(String.format("simpleClick on ['%s'] button",getName()));
		getWrappedElement().click();
		logger.info(String.format("simpleClick on ['%s'] button",getName()));
		return this;
	}
}
