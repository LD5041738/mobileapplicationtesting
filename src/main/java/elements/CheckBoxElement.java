package elements;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import driver.BasePage;

public class CheckBoxElement extends CommonElements
{
	static String nameOfElement;
	private static Logger logger = Logger.getLogger(ActionsClass.class);
	public CheckBoxElement(WebElement element, String elementName) 
	{
		super(element, elementName);
	}
	
	public boolean isChecked()
	{
		int returnSize=BasePage.driver.findElements(By.xpath("//span[text()='"+nameOfElement+"']//preceding-sibling::span[@class='rct-checkbox']//child::*[@class='rct-icon rct-icon-check']")).size();
		if(returnSize>0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void checkBox()
	{
		nameOfElement=getName();
		if(isChecked())
		{
			logger.info("Check box already checked");
			Assert.assertTrue(false);
		}
		else
		{
			getWrappedElement().click();
			logger.info("Check box checked");
		}
	}
	
	public void uncheckBox()
	{
		nameOfElement=getName();
		if(isChecked())
		{
			logger.info("Check box unchecked");
			getWrappedElement().click();
		}
		else
		{
			logger.info("Check box already unchecked");
			Assert.assertTrue(false);
		}
	}
}
