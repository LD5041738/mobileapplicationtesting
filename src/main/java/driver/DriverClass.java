package driver;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import com.gargoylesoftware.htmlunit.BrowserVersion;

import exceptions.UserDefineException;
import utils.Constants;

public class DriverClass 
{
	private static Logger logger = Logger.getLogger(DriverClass.class);
	public static WebDriver driver;
	public static String browserOption;
	
	public static String vmArgumentValidator()
	{
		browserOption = System.getProperty("Browser");
		if(browserOption!=null)
		{
			logger.info("Jenkins set Browser as "+browserOption+". This job has been executed by jenkins");
			return browserOption;
		}
		else
		{
			logger.info("This job has been executed on local machine");
			return browserOption=Constants.browser;
		}
	}
	public static WebDriver driverSetup()
	{
		
		vmArgumentValidator();
		if(browserOption.equalsIgnoreCase("Chrome"))
		{
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-notifications");
			System.setProperty("webdriver.chrome.driver",Constants.chromePath);
			driver = new ChromeDriver(options);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
			driver.get(Constants.url);
			logger.info("Opening "+Constants.url+" on Chrome Browser");
			return driver;
		}
		else if(browserOption.equalsIgnoreCase("Firefox"))
		{
			System.setProperty("webdriver.gecko.driver", Constants.firefoxPath);
			FirefoxOptions options = new FirefoxOptions();
			options.setProfile(new FirefoxProfile());
			options.addPreference("dom.webnotifications.enabled", false);
			driver = new FirefoxDriver(options);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
			driver.get(Constants.url);
			logger.info("Opening "+Constants.url+" on Firefox Browser");
			return driver;
		}
		else if(browserOption.equalsIgnoreCase("headless"))
		{
			driver= new HtmlUnitDriver(true);
			driver.get(Constants.url);
			logger.info("Opening "+Constants.url+" on Headless Browser");
			return driver;
		}
		else if(browserOption.equalsIgnoreCase("chromeHeadless"))
		{
			driver= new HtmlUnitDriver(BrowserVersion.CHROME,true);
			driver.get(Constants.url);
			logger.info("Opening "+Constants.url+" on Headless Browser with Chrome Capabilities");
			return driver;
		}
		else if(browserOption.equalsIgnoreCase("firefoxHeadless"))
		{
			driver = new HtmlUnitDriver(BrowserVersion.FIREFOX_60,true);
			driver.get(Constants.url);
			logger.info("Opening "+Constants.url+" on Headless Browser with Firefox Capabilities");
			return driver;
		}
		return driver;
	}
	
	public static WebDriver getDriverInsance()
	{
		return driver;
	}
	
	public static void refreshPage()
	{
		driver.navigate().refresh();
		logger.info("Page Refreshed");
	}
	
	public static void navigateToPreviousPage(String value)
	{
		try
		{
			if(value.equalsIgnoreCase("Next"))
			{
				driver.navigate().forward();
				logger.info("Navigated to Next page");
			}
			else if(value.equalsIgnoreCase("Previous"))
			{
				driver.navigate().back();
				logger.info("Navigated to Previous page");
			}
			else
			{
				logger.info("Browser Navigation option has been been chosen properly, Select 'Next' or 'Previous'");
				throw new UserDefineException("Navigation is not chosen correct - enter 'Next' or 'Previous'");
			}
		}
		catch (UserDefineException e) 
		{
			System.out.println(e.getMessage());
		}
	}
	
	
}
