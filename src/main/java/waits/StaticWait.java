package waits;

public class StaticWait 
{
	public static void staticWait(int seconds)
	{
		int second=seconds;
		int milliSeconds=second*1000;
		try 
		{
			Thread.sleep(milliSeconds);
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
	}
}
