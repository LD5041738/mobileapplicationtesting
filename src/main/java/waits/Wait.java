package waits;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import driver.BasePage;

public class Wait extends BasePage
{
	WebDriverWait wait = new WebDriverWait(driver,15);
	
	public static Wait getInstance()
	{
		return new Wait();
	}
	
	public Wait waitUntilElementVisible(WebElement element)
	{
		wait.until(ExpectedConditions.visibilityOf(element));
		return this;
	}
	
	public Wait waitUntilElementToBeClickable(WebElement element)
	{
		wait.until(ExpectedConditions.elementToBeClickable(element));
		return this;
	}
}
