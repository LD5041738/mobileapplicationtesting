package listeners;

import java.lang.reflect.Method;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class ITestListnerClass implements ITestListener
{
	Method method;

	@Override
	public void onTestStart(ITestResult result) 
	{
		System.out.println(method+" execution Started");
	}

	@Override
	public void onTestSuccess(ITestResult result) 
	{
		System.out.println(method+" executed sucessfully");
	}

	@Override
	public void onTestFailure(ITestResult result) 
	{
		System.out.println(method+" execution fail");
	}

	@Override
	public void onTestSkipped(ITestResult result) 
	{
		System.out.println(method+" execution skipped");	
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) 
	{
		
	}

	@Override
	public void onStart(ITestContext context) 
	{
		System.out.println("On Start");
	}

	@Override
	public void onFinish(ITestContext context) 
	{
		System.out.println("On Finish");
	}

}
