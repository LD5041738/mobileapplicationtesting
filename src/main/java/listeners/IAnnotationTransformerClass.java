package listeners;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.testng.IAnnotationTransformer;
import org.testng.annotations.ITestAnnotation;

public class IAnnotationTransformerClass implements IAnnotationTransformer 
{
	//This method will add annotation to all the @Test method of the class which is declared in testng.xml file.
	//Its not practical to add Test(retryAnalyzer = IRetryAnalyzerClass.class) for every TC.
	@Override
	public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) 
	{
		annotation.setRetryAnalyzer(IRetryAnalyzerClass.class);
	}

}
