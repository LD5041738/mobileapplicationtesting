package pages.elementpage;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.aventstack.extentreports.Status;
import driver.BasePage;
import elements.ButtonOrLinkElement;
import exceptions.UserDefineException;
import reporter.ExtendReport;
import waits.Wait;

public class LinkesPage extends BasePage
{
	@FindBy(xpath = "//div[text()='Links']") WebElement pageTitle;
	
	ExtendReport ex= new ExtendReport();
	
	private LinkesPage()
	{
		PageFactory.initElements(driver,this);
	}
	
	public static LinkesPage getInstance()
	{
		return new LinkesPage();
	}
	
	//---------------------------------------Validation on page ------------------------------------------------
	
	public LinkesPage getPageTitle() throws UserDefineException
	{
		Wait.getInstance().waitUntilElementVisible(pageTitle);
		if(pageTitle.getText().equals("Links"))
		{
			System.out.println("Element is visible");
			ex.login.log(Status.PASS, "Title Validated");
		}
		else
		{
			ex.login.log(Status.FAIL, "Title Validation fail");
			throw new UserDefineException("Element not visible");
		}
		return this;
	}
	
	//------------------------------------------Operations on page ------------------------------------------------
	
	
}
