package pages.elementpage;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import driver.BasePage;
import elements.ButtonOrLinkElement;
import elements.InputElement;
import exceptions.UserDefineException;
import reporter.ExtendReport;
import utils.DataObject;
import utils.WebTablePOJO;
import waits.Wait;

public class WebTablePage extends BasePage
{
	@FindBy(xpath = "//div[text()='Web Tables']") WebElement pageTitle;
	@FindBy(xpath = "//button[@id='addNewRecordButton']") WebElement addButton;
	@FindBy(xpath = "//div//label[@id='firstName-label']/following::div//input[@id='firstName']") WebElement rfFirstName;
	@FindBy(xpath = "//div//label[@id='lastName-label']/following::div//input[@id='lastName']") WebElement rfLastName;
	@FindBy(xpath = "//div//label[@id='userEmail-label']/following::div//input[@id='userEmail']") WebElement rfEmail;
	@FindBy(xpath = "//div//label[@id='age-label']/following::div//input[@id='age']") WebElement rfAge;
	@FindBy(xpath = "//div//label[@id='salary-label']/following::div//input[@id='salary']") WebElement rfSalary;
	@FindBy(xpath = "//div//label[@id='department-label']/following::div//input[@id='department']") WebElement rfDepartment;
	@FindBy(xpath = "//button[@id='submit']") WebElement rfSubmitButton;
	@FindBy(xpath = "//input[@id='searchBox']") WebElement seachOperation;
	
	ExtendReport ex= new ExtendReport();
	
	WebTablePOJO wtp= new WebTablePOJO();
	List<WebTablePOJO> list= new ArrayList<WebTablePOJO>();
	
	private WebTablePage()
	{
		PageFactory.initElements(driver,this);
	}
	
	public static WebTablePage getInstance()
	{
		return new WebTablePage();
	}
	
	//---------------------------------------Validation on page ------------------------------------------------
	
	public WebTablePage getPageTitle() throws UserDefineException
	{
		Wait.getInstance().waitUntilElementVisible(pageTitle);
		if(pageTitle.getText().equals("Web Tables"))
		{
			System.out.println("Element is visible");
			ex.login.log(Status.PASS, "Title Validated");
		}
		else
		{
			ex.login.log(Status.FAIL, "Title Validation fail");
			throw new UserDefineException("Element not visible");
		}
		return this;
	}
	
	//------------------------------------------Operations on page ------------------------------------------------
	
	public WebTablePage addValueIntoWebTable()
	{
		new ButtonOrLinkElement(addButton,"Add Button").click();
		ex.login.log(Status.PASS, "Clicked on Add Button");
		return this;
	}
	
	public WebTablePage setFirstNameInRegestrationForm(String firstNameValue)
	{
		wtp.setFirstName(firstNameValue);
		list.add(wtp);
		new InputElement(rfFirstName,wtp.getFirstName()).setValue(wtp.getFirstName());
		ex.login.log(Status.PASS, firstNameValue+" is set as First Name in Regestration Form");
		return this;
	}
	
	public WebTablePage setLastNameInRegestrationForm(String lastNameValue)
	{
		wtp.setLastName(lastNameValue);
		list.add(wtp);
		new InputElement(rfLastName,wtp.getLastName()).setValue(wtp.getLastName());
		ex.login.log(Status.PASS, lastNameValue+" is set as Last Name in Regestration Form");
		return this;
	}
	
	public WebTablePage setEmailInRegestrationForm(String emailValue)
	{
		wtp.setEmail(emailValue);
		list.add(wtp);
		new InputElement(rfEmail, wtp.getEmail()).setValue(wtp.getEmail());
		ex.login.log(Status.PASS, emailValue+" is set as Email in Regestration Form");
		return this;
	}
	
	public WebTablePage setAgeInRegestrationForm(String ageValue)
	{
		wtp.setAge(ageValue);
		list.add(wtp);
		new InputElement(rfAge, wtp.getAge()).setValue(wtp.getAge());
		ex.login.log(Status.PASS, ageValue+" is set as Age in Regestration Form");
		return this;
	}
	
	public WebTablePage setSalaryInRegestrationForm(String salaryValue)
	{
		wtp.setSalary(salaryValue);
		list.add(wtp);
		new InputElement(rfSalary, wtp.getSalary()).setValue(wtp.getSalary());
		ex.login.log(Status.PASS, salaryValue+" is set as Salary in Regestration Form");
		return this;
	}
	
	public WebTablePage setDepartmentInRegestrationForm(String departmentValue)
	{
		wtp.setDepartment(departmentValue);
		list.add(wtp);
		new InputElement(rfDepartment,wtp.getDepartment()).setValue(wtp.getDepartment());
		ex.login.log(Status.PASS, departmentValue+" is set as Department in Regestration Form");
		return this;
	}
	
	public WebTablePage clickSubmitButton()
	{
		new ButtonOrLinkElement(rfSubmitButton,"Submit Button").click();
		ex.login.log(Status.PASS, "Clicked on Submit Button");
		return this;
	}
	
	public WebTablePage searchOperation(String searchValue)
	{
		new InputElement(seachOperation, "Searching").setValue(searchValue);
		ex.login.log(Status.PASS, "Searched for "+searchValue);
		return this;
	}
	
	//------------------------------------------Post operation Validation ------------------------------------------------
	
	public WebTablePage validateInputs()
	{	
		for(WebTablePOJO data:list)
		{
			System.out.println("FirstName :"+data.getFirstName()+" LastName :"+data.getLastName()+" Email :"+data.getEmail()+" Age :"+data.getAge()+" Salary :"+data.getSalary()+" Department :"+data.getDepartment());
			ex.login.log(Status.PASS, "Validation done :"+"FirstName :"+data.getFirstName()+" LastName :"+data.getLastName()+" Email :"+data.getEmail()+" Age :"+data.getAge()+" Salary :"+data.getSalary()+" Department :"+data.getDepartment());
		}
		return this;
	}
	
}
