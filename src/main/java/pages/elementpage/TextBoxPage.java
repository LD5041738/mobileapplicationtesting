package pages.elementpage;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import driver.BasePage;
import elements.ButtonOrLinkElement;
import elements.InputElement;
import exceptions.UserDefineException;
import reporter.ExtendReport;
import utils.Scroller;
import waits.StaticWait;
import waits.Wait;

public class TextBoxPage extends BasePage
{
	@FindBy(xpath = "//div[text()='Text Box']") WebElement pageTitle;
	@FindBy(xpath = "//input[@id='userName']") WebElement fullNameTextBox;
	@FindBy(xpath = "//input[@id='userEmail']") WebElement emailTextBox;
	@FindBy(xpath = "//textarea[@id='currentAddress']") WebElement currentAddressTextBox;
	@FindBy(xpath = "//textarea[@id='permanentAddress']") WebElement permenantAddressTextBox;
	@FindBy(xpath = "//button[@id='submit']") WebElement submitButton;
	@FindBy(xpath = "//p[@id='name']")WebElement firstValidation;
	@FindBy(xpath = "//p[@id='email']")WebElement secondValidation;
	@FindBy(xpath = "//p[@id='currentAddress']")WebElement thirdValidation;
	@FindBy(xpath = "//p[@id='permanentAddress']")WebElement forthValidation;
	
	ExtendReport ex= new ExtendReport();
	
	private static String fullName;
	private static String emailAddress;
	private static String currentAddress;
	private static String permenantAddress;
	
	private TextBoxPage()
	{
		PageFactory.initElements(driver,this);
	}
	
	public static TextBoxPage getInstance()
	{
		return new TextBoxPage();
	}
	
	//---------------------------------------Validation on page ------------------------------------------------
	
	public TextBoxPage getPageTitle() throws UserDefineException
	{
		Wait.getInstance().waitUntilElementVisible(pageTitle);
		if(pageTitle.getText().equals("Text Box"))
		{
			System.out.println("Element is visible");
			ex.login.log(Status.PASS, "Title Validated");
		}
		else
		{
			ex.login.log(Status.FAIL, "Title Validation fail");
			throw new UserDefineException("Element not visible");
		}
		return this;
	}
	
	//------------------------------------------Operations on page ------------------------------------------------
	
	public TextBoxPage sendValueToFullNameTextBox(String value)
	{
		fullName=value;
		Scroller.getInstance().scrollUntilElementIsNotVisible(fullNameTextBox);
		Wait.getInstance().waitUntilElementVisible(fullNameTextBox);
		new InputElement(fullNameTextBox, "Full Name").setValue(value);
		ex.login.log(Status.PASS, value+" send to Full Name");
		return this;
	}
	
	public TextBoxPage sendValueToEmailTextBox(String value)
	{
		emailAddress=value;
		Scroller.getInstance().scrollUntilElementIsNotVisible(emailTextBox);
		Wait.getInstance().waitUntilElementVisible(emailTextBox);
		new InputElement(emailTextBox, "Email Address").setValue(value);
		ex.login.log(Status.PASS, value+" send to Email Address");
		return this;
	}
	
	public TextBoxPage sendValueToCurrentAddressTextBox(String value)
	{
		currentAddress=value;
		Scroller.getInstance().scrollUntilElementIsNotVisible(currentAddressTextBox);
		Wait.getInstance().waitUntilElementVisible(currentAddressTextBox);
		new InputElement(currentAddressTextBox, "Current Address").setValue(value);
		ex.login.log(Status.PASS, value+" send to Current Address");
		return this;
	}
	
	public TextBoxPage sendValueToPermenantAddressTextBox(String value)
	{
		permenantAddress=value;
		Scroller.getInstance().scrollUntilElementIsNotVisible(permenantAddressTextBox);
		Wait.getInstance().waitUntilElementVisible(permenantAddressTextBox);
		new InputElement(permenantAddressTextBox, "Permenant Address").setValue(value);
		ex.login.log(Status.PASS, value+" send to Permenant Address");
		return this;
	}
	
	public TextBoxPage clickSubmitButton()
	{
		Scroller.getInstance().scrollUntilElementIsNotVisible(submitButton);
		Wait.getInstance().waitUntilElementVisible(submitButton);
		new ButtonOrLinkElement(submitButton, "Submit Button").click();
		ex.login.log(Status.PASS, "Clicked on Submit button");
		return this;
	}
	
	//------------------------------------------Post operation Validation ------------------------------------------------
	
	public TextBoxPage validateInputs()
	{
		LinkedHashMap<String, String> validator= new LinkedHashMap<>();
		
		String first[]=firstValidation.getText().trim().split(":");
		String second[]=secondValidation.getText().trim().split(":");
		String third[]=thirdValidation.getText().trim().split(":");
		String fourth[]=forthValidation.getText().trim().split(":");
		validator.put(first[0], first[1]);
		validator.put(second[0], second[1]);
		validator.put(third[0], third[1]);
		validator.put(fourth[0], fourth[1]);
		
		Set<String> iterator = validator.keySet();
		for(String value:iterator)
		{
			if(value.equalsIgnoreCase("Name"))
			{
				String actual=fullName;
				String expected=validator.get(value);
				Assert.assertEquals(actual, expected);
				ex.login.log(Status.PASS, "Name validated");
			}
			else if(value.equalsIgnoreCase("Email"))
			{
				String actual=emailAddress;
				String expected=validator.get(value);
				Assert.assertEquals(actual, expected);
				ex.login.log(Status.PASS, "Email validated");
			}
			else if(value.trim().equalsIgnoreCase("Current Address"))
			{
				String actual=currentAddress;
				String expected=validator.get(value);
				Assert.assertEquals(actual, expected);
				ex.login.log(Status.PASS, "Current Address validated");
			}
			else if(value.trim().equalsIgnoreCase("Permananet Address"))
			{
				String actual=permenantAddress;
				String expected=validator.get(value);
				Assert.assertEquals(actual, expected);
				ex.login.log(Status.PASS, "Permananet Address validated");
			}
		}
		return this;
	}
}
