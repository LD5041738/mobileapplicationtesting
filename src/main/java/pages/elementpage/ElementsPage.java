package pages.elementpage;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import driver.BasePage;
import elements.ButtonOrLinkElement;
import exceptions.UserDefineException;
import reporter.ExtendReport;
import utils.Scroller;
import waits.Wait;

public class ElementsPage extends BasePage
{
	@FindBy(xpath = "//div[@class='main-header']") WebElement pageTitle;
	@FindBy(xpath = "//div[text()='Please select an item from left to start practice.']") WebElement pageInitialMessage;
	@FindBy(xpath = "//span[text()='Text Box']") WebElement textBoxOption;
	@FindBy(xpath = "//span[text()='Check Box']") WebElement checkBoxOption;
	@FindBy(xpath = "//span[text()='Radio Button']") WebElement radioButtonOption;
	@FindBy(xpath = "//span[text()='Web Tables']") WebElement webTableOption;
	@FindBy(xpath = "//span[text()='Buttons']") WebElement buttonsOption;
	@FindBy(xpath = "//span[text()='Links']") WebElement linksOption;
	@FindBy(xpath = "//span[text()='Broken Links - Images']") WebElement brokenLinksImagesOption;
	@FindBy(xpath = "//span[text()='Upload and Download']") WebElement uploadAndDownloadOption;
	@FindBy(xpath = "//span[text()='Dynamic Properties']") WebElement DynamicPropertiesOption;
	
	ExtendReport ex= new ExtendReport();
	
	private ElementsPage()
	{
		PageFactory.initElements(driver,this);
	}
	
	public static ElementsPage getInstance()
	{
		return new ElementsPage();
	}
	
	//---------------------------------------Validation on page ------------------------------------------------
	
	public ElementsPage getPageTitle() throws UserDefineException
	{
		Wait.getInstance().waitUntilElementVisible(pageTitle);
		if(pageTitle.getText().equals("Elements"))
		{
			System.out.println("Element is visible");
			ex.login.log(Status.PASS, "Title Validated");
		}
		else
		{
			ex.login.log(Status.FAIL, "Title Validation fail");
			throw new UserDefineException("Element not visible");
		}
		return this;
	}
	
	public ElementsPage getPageInitialMessage() throws UserDefineException
	{
		Wait.getInstance().waitUntilElementVisible(pageInitialMessage);
		if(pageInitialMessage.getText().equals("Please select an item from left to start practice."))
		{
			System.out.println("PageInitialMessage is visible");
			ex.login.log(Status.PASS, "PageInitialMessage is visible");
		}
		else
		{
			ex.login.log(Status.FAIL, "PageInitialMessage not visible");
			throw new UserDefineException("PageInitialMessage not visible");
		}
		return this;
	}
	
	//------------------------------------------Operations on page ------------------------------------------------
	
	public ElementsPage clickTextBoxOption()
	{
		Scroller.getInstance().scrollUntilElementIsNotVisible(textBoxOption);
		Wait.getInstance().waitUntilElementVisible(textBoxOption);
		new ButtonOrLinkElement(textBoxOption, "Text Box Option").click();
		ex.login.log(Status.PASS, "Clicked on 'Text Box' option");
		return this;
	}
	
	public ElementsPage clickCheckBoxOption()
	{
		Scroller.getInstance().scrollUntilElementIsNotVisible(checkBoxOption);
		Wait.getInstance().waitUntilElementVisible(checkBoxOption);
		new ButtonOrLinkElement(checkBoxOption, "Check Box Option").click();
		ex.login.log(Status.PASS, "Clicked on 'Check Box' option");
		return this;
	}
	
	public ElementsPage clickRadioButtonOption()
	{
		Scroller.getInstance().scrollUntilElementIsNotVisible(radioButtonOption);
		Wait.getInstance().waitUntilElementVisible(radioButtonOption);
		new ButtonOrLinkElement(radioButtonOption, "Radio Button Option").click();
		ex.login.log(Status.PASS, "Clicked on 'Radio Button' option");
		return this;
	}
	
	public ElementsPage clickWebTableOption()
	{
		Scroller.getInstance().scrollUntilElementIsNotVisible(webTableOption);
		Wait.getInstance().waitUntilElementVisible(webTableOption);
		new ButtonOrLinkElement(webTableOption, "Web Table Option").click();
		ex.login.log(Status.PASS, "Clicked on 'Web Table' option");
		return this;
	}
	
	public ElementsPage clickButtonsOption()
	{
		Scroller.getInstance().scrollUntilElementIsNotVisible(buttonsOption);
		Wait.getInstance().waitUntilElementVisible(buttonsOption);
		new ButtonOrLinkElement(buttonsOption, "Buttons Option").click();
		ex.login.log(Status.PASS, "Clicked on 'Buttons' option");
		return this;
	}
	
	public ElementsPage clickLinksOption()
	{
		Scroller.getInstance().scrollUntilElementIsNotVisible(linksOption);
		Wait.getInstance().waitUntilElementVisible(linksOption);
		new ButtonOrLinkElement(linksOption, "Links Option").click();
		ex.login.log(Status.PASS, "Clicked on 'Buttons' option");
		return this;
	}
	
	public ElementsPage clickBrokenLinksImagesOption()
	{
		Scroller.getInstance().scrollUntilElementIsNotVisible(brokenLinksImagesOption);
		Wait.getInstance().waitUntilElementVisible(brokenLinksImagesOption);
		new ButtonOrLinkElement(brokenLinksImagesOption, "Broken Links Option").click();
		ex.login.log(Status.PASS, "Clicked on 'Broken Links' option");
		return this;
	}
	
	public ElementsPage clickUploadAndDownloadOption()
	{
		Scroller.getInstance().scrollUntilElementIsNotVisible(uploadAndDownloadOption);
		Wait.getInstance().waitUntilElementVisible(uploadAndDownloadOption);
		new ButtonOrLinkElement(uploadAndDownloadOption, "Upload and Download Option").click();
		ex.login.log(Status.PASS, "Clicked on 'Upload and Download' option");
		return this;
	}
	
	public ElementsPage clickDynamicPropertiesOption()
	{
		Scroller.getInstance().scrollUntilElementIsNotVisible(DynamicPropertiesOption);
		Wait.getInstance().waitUntilElementVisible(DynamicPropertiesOption);
		new ButtonOrLinkElement(DynamicPropertiesOption, "Dynamic Properties Option").click();
		ex.login.log(Status.PASS, "Clicked on 'Dynamic Properties' option");
		return this;
	}
}
