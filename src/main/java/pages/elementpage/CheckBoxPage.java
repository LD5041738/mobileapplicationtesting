package pages.elementpage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import driver.BasePage;
import elements.ButtonOrLinkElement;
import elements.CheckBoxElement;
import exceptions.UserDefineException;
import reporter.ExtendReport;
import waits.StaticWait;
import waits.Wait;

public class CheckBoxPage extends BasePage
{
	String boxName;
	
	@FindBy(xpath = "//div[text()='Check Box']") WebElement pageTitle;
	@FindBy(xpath = "//button[@title='Expand all']") WebElement expandButton;
	@FindBy(xpath = "//button[@title='Collapse all']") WebElement collapseButton;
	
	ExtendReport ex= new ExtendReport();
	WebElement selectCheckBox;
	
	private CheckBoxPage()
	{
		PageFactory.initElements(driver,this);
	}
	
	public static CheckBoxPage getInstance()
	{
		return new CheckBoxPage();
	}
	
	//---------------------------------------Validation on page ------------------------------------------------
	
	public CheckBoxPage getPageTitle() throws UserDefineException
	{
		Wait.getInstance().waitUntilElementVisible(pageTitle);
		if(pageTitle.getText().equals("Check Box"))
		{
			System.out.println("Element is visible");
			ex.login.log(Status.PASS, "Title Validated");
		}
		else
		{
			ex.login.log(Status.FAIL, "Title Validation fail");
			throw new UserDefineException("Element not visible");
		}
		return this;
	}
	
	//------------------------------------------Operations on page ------------------------------------------------
	
	public CheckBoxPage expandOptions()
	{
		new ButtonOrLinkElement(expandButton, "Expand Button").click();
		StaticWait.staticWait(2);
		ex.login.log(Status.PASS, "Clicked on 'Expand' option");
		return this;
	}
	
	public CheckBoxPage collapseOptions()
	{
		new ButtonOrLinkElement(collapseButton, "Collapse Button").click();
		StaticWait.staticWait(2);
		ex.login.log(Status.PASS, "Clicked on 'Collapse' option");
		return this;
	}
	
	public CheckBoxPage selectCheckBox(String boxName)
	{
		this.boxName=boxName;
		selectCheckBox=driver.findElement(By.xpath("//span[text()='"+boxName+"']/..//span[@class='rct-checkbox']"));
		new CheckBoxElement(selectCheckBox,boxName);
		ex.login.log(Status.PASS, boxName+" chcekbox is chekcked");
		return this;
	}
	
	public CheckBoxPage deselectCheckBox(String boxName)
	{
		this.boxName=boxName;
		selectCheckBox=driver.findElement(By.xpath("//span[text()='"+boxName+"']/..//span[@class='rct-checkbox']"));
		new CheckBoxElement(selectCheckBox,boxName);
		ex.login.log(Status.PASS, boxName+" chcekbox is dechekcked");
		return this;
	}
}
