package pages.homepage;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import driver.BasePage;
import elements.ButtonOrLinkElement;
import elements.InputElement;
import reporter.ExtendReport;
import utils.HideAds;
import utils.Scroller;
import waits.StaticWait;
import waits.Wait;

public class HomePage extends BasePage 
{
	@FindBy(xpath = "//*[text()='Elements']")WebElement elements;
	@FindBy(xpath = "(//div[@class='category-cards']//div[@class='card mt-4 top-card'])[2]//div/following-sibling::div[@class='card-body']\")")WebElement forms;
	@FindBy(xpath = "(//div[@class='category-cards']//div[@class='card mt-4 top-card'])[3]//div/following-sibling::div[@class='card-body']\")")WebElement alertsFrameWindows;
	@FindBy(xpath = "(//div[@class='category-cards']//div[@class='card mt-4 top-card'])[4]//div/following-sibling::div[@class='card-body']\")")WebElement widgets;
	@FindBy(xpath = "(//div[@class='category-cards']//div[@class='card mt-4 top-card'])[5]//div/following-sibling::div[@class='card-body']\")")WebElement interactions;
	@FindBy(xpath = "(//div[@class='category-cards']//div[@class='card mt-4 top-card'])[6]//div/following-sibling::div[@class='card-body']\")")WebElement bookStoreApplication;
	
	ExtendReport ex= new ExtendReport();
	
	private HomePage()
	{
		PageFactory.initElements(driver, this);
	}
	
	public static HomePage getInstance()
	{
		return new HomePage();
	}
	
	public HomePage setReport(String testCaseID)
	{
		
		ex.login=ex.extent.createTest(testCaseID);
		return this;
	}
	
	//---------------------------------------Validation on page ------------------------------------------------
	
	public HomePage homePageValidator()
	{
		if(driver.getTitle().equalsIgnoreCase("ToolsQA"))
		{
			System.out.println("Title valid");
			ex.login.log(Status.PASS, "Title Validated");
			HideAds.hideAds();
		}
		else
		{
			System.out.println("Invalid Title");
			ex.login.log(Status.FAIL, "Title Validation fail");
		}
		return this;
	}
	
	public List<String> operationCanPerform()
	{
		int size=driver.findElements(By.xpath("//div[@class='category-cards']//div[@class='card mt-4 top-card']")).size();
		System.out.println("Operation can be performed ="+size);
		List<String> operationName = new ArrayList<String>();
		for(int i=1;i<=size;i++)
		{
			operationName.add(driver.findElement(By.xpath("(//div[@class='category-cards']//div[@class='card mt-4 top-card'])["+i+"]//div/following-sibling::div[@class='card-body']")).getText());
		}
		ex.login.log(Status.PASS, "Operations perforded"+operationName.toString());
		return operationName;
	}
	
	//------------------------------------------Operations on page ------------------------------------------------
	
	public HomePage clickElements()
	{
		Scroller.getInstance().scrollUntilElementIsNotVisible(elements);
		Wait.getInstance().waitUntilElementVisible(elements);
		new ButtonOrLinkElement(elements, "Elements").click();
		ex.login.log(Status.PASS, "Clicked on 'Element' option");
		return this;
	}
	
	public HomePage clickForms()
	{
		Scroller.getInstance().scrollUntilElementIsNotVisible(forms);
		Wait.getInstance().waitUntilElementVisible(forms);
		new ButtonOrLinkElement(forms, "Forms").click();
		ex.login.log(Status.PASS, "Clicked on 'Forms' option");
		return this;
	}
	
	public HomePage clickAlertsFrameWindows()
	{
		Scroller.getInstance().scrollUntilElementIsNotVisible(alertsFrameWindows);
		Wait.getInstance().waitUntilElementVisible(alertsFrameWindows);
		new ButtonOrLinkElement(alertsFrameWindows, "Alerts,Frame & Windows").click();
		ex.login.log(Status.PASS, "Clicked on 'Alerts,Frame & Windows' option");
		return this;
	}
	
	public HomePage clickWidgets()
	{
		Scroller.getInstance().scrollUntilElementIsNotVisible(widgets);
		Wait.getInstance().waitUntilElementVisible(widgets);
		new ButtonOrLinkElement(widgets, "Widgets").click();
		ex.login.log(Status.PASS, "Clicked on 'Widgets' option");
		return this;
	}
	
	public HomePage clickInteractions()
	{
		Scroller.getInstance().scrollUntilElementIsNotVisible(interactions);
		Wait.getInstance().waitUntilElementVisible(interactions);
		new ButtonOrLinkElement(interactions, "Interactions").click();
		ex.login.log(Status.PASS, "Clicked on 'Interactions' option");
		return this;
	}
	
	public HomePage clickBookStoreApplication()
	{
		Scroller.getInstance().scrollUntilElementIsNotVisible(bookStoreApplication);
		Wait.getInstance().waitUntilElementVisible(bookStoreApplication);
		new ButtonOrLinkElement(bookStoreApplication, "Book Store Application").click();
		ex.login.log(Status.PASS, "Clicked on 'Interactions' option");
		return this;
	}
}
