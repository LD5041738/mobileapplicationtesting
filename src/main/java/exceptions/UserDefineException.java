package exceptions;

public class UserDefineException extends Exception 
{
	public UserDefineException(String value)
	{
		super(value);
	}
}
