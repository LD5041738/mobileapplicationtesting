package reporter;

public class ReportTestInit 
{
	ExtendReport ex= new ExtendReport();
	
	public static ReportTestInit getInstance()
	{
		return new ReportTestInit();
	}
	
	public void reportInit(String testCaseID)
	{
		ex.login=ex.extent.createTest(testCaseID);
	}
}
