package suite;

import java.net.UnknownHostException;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import driver.DriverClass;
import reporter.ExtendReport;


public class TestSuiteConfiguration 
{
	private static Logger logger = Logger.getLogger(TestSuiteConfiguration.class);
	static WebDriver driver;
	static String ReportName;
	
	@BeforeSuite
	public static void beforeSuite() 
	{
		DOMConfigurator.configure("log4j.xml");
	}
	
	@BeforeTest
	public static void beforeTest() throws UnknownHostException
	{
		driver=DriverClass.driverSetup();
		logger.info("Browser initilization sucessful");
		ReportName=ExtendReport.getInstance().startReport();
	}
	
	@BeforeClass
	public static void beforeClass()
	{
		
	}
	
	@BeforeMethod
	public static void beforeMethod()
	{
		
	}
	
	@AfterMethod
	public static void afterMethod(ITestResult result)
	{
		if(result.getStatus() == ITestResult.FAILURE) 
        {
        	ExtendReport.login.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" FAILED ", ExtentColor.RED));
        	ExtendReport.login.fail(result.getThrowable());
        }
        else if(result.getStatus() == ITestResult.SUCCESS) 
        {
        	ExtendReport.login.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" PASSED ", ExtentColor.GREEN));
        }
        else 
        {
        	ExtendReport.login.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" SKIPPED ", ExtentColor.ORANGE));
        	ExtendReport.login.skip(result.getThrowable());
        }
	}
	
	@AfterClass
	public static void afterClass()
	{
		
	}
	
	@AfterTest
	public static void afterTest()
	{
		ExtendReport.getInstance().endReport();
	}
	
	@AfterSuite
	public static void afterSuite()
	{
		driver.quit();
	}
}
