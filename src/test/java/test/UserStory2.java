package test;

import java.io.IOException;
import java.util.List;

import org.testng.annotations.Test;

import exceptions.UserDefineException;
import pages.elementpage.ElementsPage;
import pages.elementpage.TextBoxPage;
import pages.homepage.HomePage;
import reporter.ReportTestInit;
import suite.TestSuiteConfiguration;
import utils.DataObject;
import utils.ReadExcel;

//text Box Page Validator
public class UserStory2 extends TestSuiteConfiguration
{
	
	static String className= UserStory2.class.getName().replace("test.","");
	
	//Validate if Element option is clickable on homepage, Text Box Option is clickble and perform operation in it. 
	@Test
	public static void TC_01() throws IOException, UserDefineException
	{
		System.out.println("<--------------------------UserStory2-------------------------------------------------------->");
		String testCaseName=new Object(){}.getClass().getEnclosingMethod().getName();
		ReadExcel.getInstance().checkRunStatus(className, testCaseName);
		ReportTestInit.getInstance().reportInit(className+"-"+testCaseName);
		
		HomePage.getInstance()
		.homePageValidator()
		.clickElements();
		
		ElementsPage.getInstance().
		getPageTitle().
		getPageInitialMessage().
		clickTextBoxOption();
		
		TextBoxPage.getInstance().
		getPageTitle().
		sendValueToFullNameTextBox(DataObject.getVariable("Name", testCaseName)).
		sendValueToEmailTextBox(DataObject.getVariable("Email", testCaseName)).
		sendValueToCurrentAddressTextBox(DataObject.getVariable("CurrentAddress", testCaseName)).
		sendValueToPermenantAddressTextBox(DataObject.getVariable("PermanantAddress", testCaseName)).
		clickSubmitButton().
		validateInputs();
		
	}
}
