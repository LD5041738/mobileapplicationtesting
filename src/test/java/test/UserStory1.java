package test;

import java.io.IOException;
import java.util.List;

import org.testng.annotations.Test;

import pages.homepage.HomePage;
import reporter.ReportTestInit;
import suite.TestSuiteConfiguration;
import utils.DataObject;
import utils.ReadExcel;

//Home Page Validator
public class UserStory1 extends TestSuiteConfiguration
{
	static String className= UserStory1.class.getName().replace("test.","");
	
	//Validate operation then can get performed
	@Test
	public static void TC_01() throws IOException
	{
		System.out.println("<--------------------------UserStory1-------------------------------------------------------->");
		String testCaseName=new Object(){}.getClass().getEnclosingMethod().getName();
		
		ReportTestInit.getInstance().reportInit(className+"-"+testCaseName);
		ReadExcel.getInstance().checkRunStatus(className, testCaseName);
		List<String> list= HomePage.getInstance().setReport(testCaseName).homePageValidator().operationCanPerform();
		System.out.println(list);
	}
	/* adding comment */
	
}
