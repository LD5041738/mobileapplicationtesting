package test;

import java.io.IOException;

import org.testng.annotations.Test;

import exceptions.UserDefineException;
import pages.elementpage.CheckBoxPage;
import pages.elementpage.ElementsPage;
import pages.elementpage.RadioButtonPage;
import reporter.ReportTestInit;
import suite.TestSuiteConfiguration;
import utils.DataObject;
import utils.ReadExcel;

public class UserStory4 extends TestSuiteConfiguration
{
	static String className= UserStory4.class.getName().replace("test.","");
	 
	@Test
	public static void TC_01() throws IOException, UserDefineException
	{
		System.out.println("<--------------------------UserStory4-------------------------------------------------------->");
		String testCaseName=new Object(){}.getClass().getEnclosingMethod().getName();
		ReadExcel.getInstance().checkRunStatus(className, testCaseName);
		ReportTestInit.getInstance().reportInit(className+"-"+testCaseName);
		
		ElementsPage.getInstance(). 
		clickRadioButtonOption();
		
		RadioButtonPage.getInstance().
		selectOption(DataObject.getVariable("RadioOption", testCaseName));
	}
}
